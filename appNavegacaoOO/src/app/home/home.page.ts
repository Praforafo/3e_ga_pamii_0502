import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    rotas = [
      {
        path: "/cad-imovel",
        text: "Cadastrar Imóvel"
      },
      {
        path: "/cad-imobiliaria",
        text: "Cadastrar Imobiliária"
      },
      {
        path: "/cad-loocador",
        text: "Cadastrar Locador"
      },
      {
        path: "/cad-proprietario",
        text: "Cadastrar Proprietário"
      }
    ]

  constructor() {}

}
